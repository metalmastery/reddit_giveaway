extends Node

# this controller works relatively to ship's rotation
# it means that joystick UP is FORWARD to current ship's position
# it requires some habit and a bit of training

# also it uses direct names for engines
# it could be fine for prototyping, but if you want to make different ships
# with different amount and configuration of engines - it could be a mess

var thrusters = {
	backward_left = 0,
	backward_right = 0,
	
	forward_left = 0,
	forward_right = 0
}

func _ready():
	pass 

# this func is for compatibility with other controllers only
func register_engine(engine):
	pass

func _unhandled_input(event):
	
	var joypad_vec = Vector2.ZERO
	
	if Input.get_connected_joypads().size() > 0:
		
		joypad_vec = Vector2(Input.get_joy_axis(0, 0), -Input.get_joy_axis(0, 1))
		
	thrusters.backward_left = 0
	thrusters.backward_right = 0
	
	thrusters.forward_left = 0
	thrusters.forward_right = 0
	
	if joypad_vec.y > 0:
		thrusters.backward_left = joypad_vec.y
		thrusters.backward_right = joypad_vec.y
		
	if joypad_vec.y < 0:
		thrusters.forward_left = -joypad_vec.y
		thrusters.forward_right = -joypad_vec.y
		
# just to make rotation not so fast
	joypad_vec *= 0.5
		
	if joypad_vec.x > 0:
		thrusters.backward_left += joypad_vec.x
		thrusters.backward_right -= joypad_vec.x
		thrusters.forward_right += joypad_vec.x
		thrusters.forward_left -= joypad_vec.x
		
	if joypad_vec.x < 0:
		thrusters.backward_left += joypad_vec.x
		thrusters.backward_right -= joypad_vec.x
		thrusters.forward_right += joypad_vec.x
		thrusters.forward_left -= joypad_vec.x

func get_thrusters_control(r, a_v):
	return thrusters
