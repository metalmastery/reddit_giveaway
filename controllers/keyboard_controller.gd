extends Node

# this controller works relatively to ship's rotation
# it means that KEY UP is FORWARD to current ship's position
# it requires some habit and a bit of training

# also it uses direct names for engines
# it could be fine for prototyping, but if you want to make different ships
# with different amount and configuration of engines - it could be a mess


var thrusters = {
	backward_left = 0,
	backward_right = 0,
	
	forward_left = 0,
	forward_right = 0
}

# this func is for compatibility with other controllers only
func register_engine(engine):
	pass

func _ready():
	pass 

func _unhandled_input(event):
	
# reset all the values
	for k in thrusters.keys():
		thrusters[k] = 0
	
# read events
	if event is InputEventKey and event.pressed:
		if event.scancode == KEY_UP:
# simply enable needed engines for every key
			thrusters.backward_left = 1
			thrusters.backward_right = 1
			
		if event.scancode == KEY_DOWN:
			thrusters.forward_left = 1
			thrusters.forward_right = 1
			
		if event.scancode == KEY_LEFT:
			thrusters.backward_right = 1
			thrusters.forward_left = 1
			
		if event.scancode == KEY_RIGHT:
			thrusters.backward_left = 1
			thrusters.forward_right = 1
		
# if you decide to mess up the values - it will be clamped for consistancy sake
	for k in thrusters.keys():
		thrusters[k] = clamp(thrusters[k], 0, 1)
			

func get_thrusters_control(r, a_v):
	return thrusters
