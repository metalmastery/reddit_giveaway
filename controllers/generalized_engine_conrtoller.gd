extends Node

# this controller works INDEPENDENTLY to ship's rotation
# it means that joystick UP is UP on the screen
# it means that KEY UP is UP on the screen
# it requires some habit and a bit of training

# also it engines auto-registering
# it means that if you want to add a bunch on engines they could be automatically
# managed - this controller will try to move the ship in the direction you want
# and align it in the same direction

# in some cases it's not possible:
# if you point ALL the engines directly OUT from center - it will be (probably) able 
# to move the ship, but not rotate it properly

# so this is the key here
var desired_direction : Vector3 = Vector3(1, 0, 0)

# here we store registered engines 
var engines = {}

func _ready():
	pass
	
# some fuckd up stuff here, hold my beer, I'll explain
func register_engine(engine):
# OK, so Godot applies linear and angular impulses independently
# so we need to know how each engine contribute to each of it

# here we calculate the proportion between X and Z axis of impulse based on engine's rotation
	var engine_thrust = Vector3(0, 0, 1).rotated(Vector3.UP, engine.rotation.y)
	
# probably it's not needed, but ok
	var linear_contribution = engine_thrust.normalized()
	
# here we understand  which direction the engine applies torque
# we don't need any values except the sign of it, so 1 or -1
# (the real value of applied implulse is based on Thruster file)
	var angular_contribution = sign(Vector3(0, 0, -1).cross(engine.translation.rotated(Vector3.UP, engine.rotation.y)).y)

# save our info about the engine
	engines[engine.get_instance_id()] = {
		rotation = engine.rotation.y,
		linear_contribution = linear_contribution,
		angular_contribution = angular_contribution
	}

func _physics_process(delta):
# in case you need it for any calibration
#	desired_direction = Vector3.ZERO

# you can control it with joystick
	desired_direction = Vector3(Input.get_joy_axis(0, 0), 0, Input.get_joy_axis(0, 1))
	
# or keyboard
	if Input.is_key_pressed(KEY_UP):
		desired_direction += Vector3(0,0,-1)
	if Input.is_key_pressed(KEY_DOWN):
		desired_direction += Vector3(0,0,1)
	if Input.is_key_pressed(KEY_LEFT):
		desired_direction += Vector3(-1,0,0)
	if Input.is_key_pressed(KEY_RIGHT):
		desired_direction += Vector3(1,0,0)
	pass

# here we try to use any engines possible to achieve desired vector 
func get_thrusters_control(r, a_v):
	
# just normalize it to make small joystick tilt more precise
	var desire_normalized = desired_direction.normalized()

# here we're taking two vectors 
# this is perpendicular to the ship's axis
	var rot = Vector3(1, 0, 0).rotated(Vector3.UP, r.y)
# and this is aligned to it
	var rot_2 = Vector3(0, 0, 1).rotated(Vector3.UP, r.y)
	
# we take dot products of them to understand how far are we from desired vector	
	var dot = rot.dot(desire_normalized)
	var dot_2 = rot_2.dot(desire_normalized)
	
# to avoid solving derivatives we can use a hack here - just subtract
# current angular velocity from dot product, the idea is this:
# if the ship is rotated at 0 degrees and desired rotation is 90 degrees
# then you need to accelerate from 0 to 45 degrees and then
# start slowing down (from 45 to 90) to achieve target 90 degrees with ZERO angular velocity
# AGAIN: it's more complicated in general, so here we just use a hack that works in some range of values
	var dot_relative = dot - a_v.y * 0.5
	
# this will contain engines IDs with their thrust values from 0 to 1
	var controls = {}

# in case of joystick we want to be able to tilt the stick gently
	var thrust = desired_direction.length()

# a bunch of variables, will be explained below
	var eng = null
	var engine_value = 0
	var rot_eng = null
	var dot_eng = 0
	var dot_linear = null
# go through every registered engine
	for k in engines.keys():
# current engine in computations
		eng = engines[k]
		
# remember linear_contribution from above? 
# here we can rotate it accordingly to ship's rotation 
# and use dot product to use only the engines that contribute in needed direction 
		dot_linear = eng.linear_contribution.rotated(Vector3.UP, r.y).dot(desire_normalized) 

# so here we start collecting values
# we start with rotation
		engine_value = eng.angular_contribution * dot_relative

# then add linear velocity
# if you comment this line and the "if" below - ship would only rotate
		engine_value += clamp(dot_linear, 0, 1)

# this is a special case for fast opposite turns
# if you go UP and press DOWN
# WITH this lines ship will turn fast and start slowing down, then start accelerating in needed direction
# WITHOUT this lines ship will slow down first, slowly rotating, start accelerating in needed direction
		if dot_2 < -0.5:
			engine_value -= dot_2 * eng.angular_contribution

# taking into account joystick tilt
		engine_value *= thrust

# ensure (0, 1) range for consistency
		engine_value = clamp(engine_value, 0, 1)
		
# assign the value to return object
		controls[k] = engine_value
	
	return controls
