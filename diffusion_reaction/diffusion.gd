extends Node2D

var current_frame = 0
# it needs a couple frames to seed the diffusion
var replace_seed_by_feedback_at_frame = 5

# you can run the reaction for some amount of frames and stop there
# or leave it at 0 to run forever
var run_for_frames = 0

# reaction runs slowly so I daisy-chained a couple viewports to speed it up
# more viewports = more runs per frame = faster reaction
var chained_viewports_count = 50
var chain_passes = []

var is_running = false
var is_preparing = false


# reaction happens in $reaction/tex shader
# $screen adds some nicer colouring

func _ready():
	
	$seed_playground/noise.texture.noise.seed = randi()
	
	var seed_p = $seed_playground
	remove_child(seed_p)
	$seed.add_child(seed_p)
	
	setup_viewport($seed, null)
	setup_texture($screen)
	
	setup_viewport($reaction, $reaction/tex)

	$reaction/tex.texture = $seed.get_texture()
	$screen.texture = $reaction.get_texture()
	$seed/tex.rect_size = get_viewport().size
	
	setup_chain()

	prepare()

func setup_viewport(vp : Viewport, tex: TextureRect):
	vp.transparent_bg = true
	vp.render_target_update_mode = Viewport.UPDATE_ALWAYS
	vp.render_target_v_flip = true
	vp.size = get_viewport().size
	
	setup_texture(tex)
	
func setup_texture(tex: TextureRect):
	if !tex:
		return
	tex.rect_size = get_viewport().size
	tex.expand = true

func _process(delta):
	current_frame += 1
	
	if is_preparing and current_frame > replace_seed_by_feedback_at_frame:
		start_diffusion()
		
	if is_running and (current_frame > run_for_frames) and !run_for_frames == 0:
		stop_diffusion()
	
	
# basically what we do here is creating a chain of viewports
# each one acts as a seed to the next one
# and the first one gets our original seed 

# we run it for a couple frames to ensure that the seed is rendered
# then we cycle the whole thing up and feed the last viewport to the first one 
# see start_diffusion() and prepare()

func setup_chain():

	for i in range(0, chained_viewports_count):

		var render_pass
		if i == 0:
			render_pass = $reaction
		else:
			render_pass = $reaction.duplicate(0)
		add_child(render_pass)
		
		render_pass.get_child(0).material = render_pass.get_child(0).material.duplicate(0)
		chain_passes.append(render_pass)
		
		var input_texture = $seed.get_texture()
		if i > 0:
			input_texture = chain_passes[i - 1].get_texture()
		
		render_pass.get_child(0).texture = input_texture
		
		render_pass.set_size(get_viewport().size)

	$screen.texture = chain_passes[chain_passes.size() - 1].get_texture()
		
func stop_diffusion():
	prints('stop_diffusion')
	
	is_running = false
	is_preparing = false
	
	for c in chain_passes:
		c.render_target_clear_mode = Viewport.CLEAR_MODE_NEVER
		c.render_target_update_mode = Viewport.UPDATE_DISABLED
		
func prepare():
	prints('prepare')
	
	current_frame = 0
	is_running = false
	is_preparing = true
	
	for c in chain_passes:
		c.render_target_clear_mode = Viewport.CLEAR_MODE_ALWAYS
		c.render_target_update_mode = Viewport.UPDATE_ALWAYS
		
	chain_passes[0].get_child(0).texture = $seed.get_texture()
#	chain_passes[chain_passes.size() - 1].render_target_clear_mode = Viewport.CLEAR_MODE_ONLY_NEXT_FRAME

func start_diffusion():
	prints('start_diffusion')
	is_running = true
	is_preparing = false
	
	chain_passes[0].get_child(0).texture = chain_passes[chain_passes.size() - 1].get_texture()
	chain_passes[chain_passes.size() - 1].render_target_clear_mode = Viewport.CLEAR_MODE_ONLY_NEXT_FRAME
