extends Node

var cell_size : float = 100
var grid_offset : float = 100

var grid_size : Vector2 = Vector2(3,3)

var grid : Array
var current_position : Vector2
var facing_direction : Vector2

onready var character = $char
onready var arrow = $char/arrow

func _ready():
	start()

func fill_grid():
	grid = []
	for i in grid_size.y:
		var row = []
		for k in grid_size.x:
			row.append('tile_' + String(i) + '_' + String(k))
			
			var s = Sprite.new()
			s.texture = load('res://icon.png')
			s.modulate = Color.red
			s.scale = Vector2.ONE * 0.3
			s.position = _grid_to_visual(Vector2(k, i))
			$tiles.add_child(s)

func _input(event):
	
	if event is InputEventKey and event.is_pressed():
		
		var dir = Vector2()
		dir.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
		dir.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up") 
		
		if facing_direction.dot(dir) == 1:
			move(facing_direction)
#			pass	
		else:
			rotate_to(dir)
			
func start():
	fill_grid()
	current_position = Vector2.ZERO
	
	facing_direction = Vector2(0,-1)
	
	move(current_position)
	rotate_to(facing_direction)
	
func move(dir : Vector2):
	
	var new_position = Vector2()
	new_position.x = abs( int(current_position.x + dir.x + grid_size.x) % int(grid_size.x) )
	new_position.y = abs ( int(current_position.y + dir.y + grid_size.y) % int(grid_size.y) )
	
	prints('move', current_position, '->', new_position)
	
	var current_visual_position = _grid_to_visual(current_position)
	
	var desired_visual_position = _grid_to_visual(new_position)
	
	var diff = dir * cell_size * 0.5
	
	$Tween.interpolate_property(character, 'position', current_visual_position, current_visual_position + diff, 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property(character, 'position', desired_visual_position - diff, desired_visual_position, 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.3)
	$Tween.start()
	
	current_position = new_position
	
func rotate_to(dir):
	var current_angle = dir_to_angle(facing_direction)
	
	var desired_angle = dir_to_angle(dir)
	
	var short_dist = short_angle_dist(current_angle, desired_angle)
	
	prints('rotate_to', current_angle, '->', desired_angle, short_dist)
	
	$Tween.interpolate_property(arrow, 'rotation', current_angle, current_angle + short_dist, 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	
	facing_direction = dir
	
func _grid_to_visual(pos : Vector2):
	var visual_position = Vector2()
	visual_position.x += grid_offset
	visual_position.x += pos.x * cell_size
	
	visual_position.y += grid_offset
	visual_position.y += pos.y * cell_size
	
	return visual_position
	
func short_angle_dist(from, to):
	
	var max_a = PI*2;
#	var da = (to - from) % max_a; 
	var da = fmod(to - from, max_a)
	return fmod(2*da, max_a) - da;
	
func dir_to_angle(dir):
	var map = {
		Vector2(0,-1) : 0,
		Vector2(1,0) : PI/2,
		Vector2(-1,0) : 3 * PI/2,
		Vector2(0,1) : PI,
	}
	
	return map[dir]
