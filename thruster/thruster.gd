extends Particles

# the only purpose for moving this code completely ou of ship
# is to make engines replaceable and to be able 
# to define each model's thrust and consumption

# visual stuff
var particle_velocity = 100

# max impulse 
var impulse = 0.1

# how much impulse to be applied (0 to 1)
var current_value = 0

func _ready():
	pass 

func fire(value):
#	remember how much we press the gas pedal
	current_value = value
#	visualize it
	process_material.initial_velocity = value * particle_velocity
#	you can go even further
#	process_material.scale = value

# simple deadzone to make it more appealing visually
	if value < 0.05:
		emitting = false
	else:
		emitting = true

func get_impulse(parent_rotation):
# so when we need to apply impulse (or force) - each engine returns its current data
# I use parent_rotation as a parameter to avoid direct parent-of-parent lookup 
	return {
		position = translation.rotated(Vector3.UP, parent_rotation),
		fuel = current_value * impulse,
		impulse = (Vector3.BACK * current_value * impulse).rotated(Vector3.UP, parent_rotation + rotation.y) 
	}
