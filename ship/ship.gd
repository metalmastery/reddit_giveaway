extends RigidBody

# uncomment one of these at the time
var Controller = preload('res://controllers/generalized_engine_conrtoller.gd')
#var Controller = preload('res://controllers/keyboard_controller.gd')
#var Controller = preload('res://controllers/joystick_controller.gd')

var contr = null

onready var spaceship = $first_spaceship
onready var spaceship_body = self

onready var forward_right_engine = $engines/forward_right
onready var forward_left_engine = $engines/forward_left
onready var backward_right_engine = $engines/backward_right
onready var backward_left_engine = $engines/backward_left

var max_velocity = 20
var max_angular_velocity = 5
var is_ready = false

var max_fuel = 1000
var fuel = max_fuel

func _ready():
	contr = Controller.new()
	add_child(contr)
	
# this is for GENERALIZED controller only, other controllers don't need it to work correctly
	for engine in $engines.get_children():
		contr.register_engine(engine)
			
	is_ready = true

func _process(delta):
	pass
 
func _physics_process(delta):
	
# just to ensure we prepared everything and ready to go
	if !is_ready:
		return
	
# this should work with KeyboardController and JoystickController
#	run_engines(delta)

# this should work with GeneralizedEngineController
	run_registered_engines(delta)
	
# limit max velocity
	if linear_velocity.length() > max_velocity:
		linear_velocity = linear_velocity.normalized() * max_velocity

# visual tilt of the model for maneurability
	spaceship.rotation.z = clamp(angular_velocity.y / 3, -PI/6, PI/6)

# limit max angular velocity
	if abs(angular_velocity.y) > max_angular_velocity:
		angular_velocity.y = sign(angular_velocity.y) * max_angular_velocity

func run_registered_engines(delta):
# so we get control values for our engines
	var controls = contr.get_thrusters_control(rotation, angular_velocity)
	var impulse = null
# take every engine (remember to register them all before)
	for engine in $engines.get_children():
# basically the same as in run_engines, but we don't care about names
		engine.fire(controls[engine.get_instance_id()])
		impulse = engine.get_impulse(rotation.y)
# you can apply_force() instead, it would be more even across the frames
# for now if you skip a frame, you skip an impulse
		apply_impulse(impulse.position, impulse.impulse)

func run_engines(delta):
# so we get control values for our engines
	var controls = contr.get_thrusters_control(rotation, angular_velocity)
	
# turn the engine to some percent of thrust based on controller decision
	backward_right_engine.fire(controls.backward_right)
# get the engine's impulse
	var impulse = backward_right_engine.get_impulse(rotation.y)
# and apply it
	apply_impulse(impulse.position, impulse.impulse)
	
# optionally consume some fuel
	fuel -= impulse.fuel
	
# ...and repeat for each engine
# it could be hard if you have different engines amount and configurations 
# on different ships

	backward_left_engine.fire(controls.backward_left)
	impulse = backward_left_engine.get_impulse(rotation.y)
	apply_impulse(impulse.position, impulse.impulse)
	
	fuel -= impulse.fuel
	
	forward_right_engine.fire(controls.forward_right)
	impulse = forward_right_engine.get_impulse(rotation.y)
	apply_impulse(impulse.position, impulse.impulse)
	
	fuel -= impulse.fuel

	forward_left_engine.fire(controls.forward_left)
	impulse = forward_left_engine.get_impulse(rotation.y)
	apply_impulse(impulse.position, impulse.impulse)
	
	fuel -= impulse.fuel
	
	
