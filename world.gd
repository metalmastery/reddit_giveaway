extends Spatial

onready var camera = $Camera
onready var ship = $ship

func _ready():
	pass
	
func _process(delta):
	
#	very simple follow camera
	var camera_target_position = Vector3.ZERO

	camera_target_position.x = ship.translation.x
	camera_target_position.z = ship.translation.z
	camera_target_position.y = 150
	
	camera_target_position.x += ship.linear_velocity.x / 1
	camera_target_position.z += ship.linear_velocity.z / 1
	
	camera_target_position.y += Vector2(ship.translation.x, ship.translation.z).distance_to(Vector2(camera.translation.x, camera.translation.z))
	
	camera_target_position = camera.translation.linear_interpolate(camera_target_position, 0.75)
	
	camera.translation = camera_target_position
	
	camera.look_at(Vector3(camera.translation.x, 0, camera.translation.z), Vector3.FORWARD)
